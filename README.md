Follow the below steps to run this website on localhost.

1. Install Python 3.4.4
2. Using pip, install python packages listed in info/pypackages.txt
3. Use Django's runserver command to launch the development server