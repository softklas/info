__author__ = 'prassanthi'
from django import forms
from enum import Enum


CATEGORIES = (
    ("",""),
    ('SCHOOL', 'SCHOOL'),
    ('WORK', 'WORK'),
    ('OTHER','OTHER')
    )
class ContactForm(forms.Form):
    contact_name = forms.CharField(required=True)
    contact_email = forms.EmailField(required=True)
    Subject = forms.TypedChoiceField(choices=CATEGORIES, coerce=str)
    # Subject_Text = forms.CharField(max_length=100,initial="_",widget=forms.TextInput(attrs={'placeholder': 'Type subject here '}))
    Subject_Text = forms.CharField(max_length=100,required=False)
    content = forms.CharField(
        required=True,
        widget=forms.Textarea
    )