__author__ = 'prassanthi'

from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.conf import settings
from softklas.model import ContactForm


def landing(request):
   return render(request, "landing.html")

def index(request):
   return render(request, "index.html")

def about_us(request):
   return render(request, "about-us.html")

# def contact_us(request):
#    return render(request, "contact-us.html")

def gallery(request):
   return render(request, "gallery.html")

def learn_with_us(request):
   return render(request, "learn-with-us.html")

def login(request):
   return render(request, "login.html")

def workshops(request):
   return render(request, "workshops.html")


def contact_us(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            from_name = form.cleaned_data['contact_name']
            subject = form.cleaned_data['Subject']
            contact_email = form.cleaned_data['contact_email']

            Message ="Name - "+ from_name +"\n"+ "Contact-Email- "+" "+ contact_email+ "\n"+"Content - " +form.cleaned_data['content']

            try:
                if subject == 'OTHER':
                    subject_text = form.cleaned_data['Subject_Text']
                    send_mail(subject_text, Message, contact_email, [settings.EMAIL_HOST_USER],fail_silently=False)
                else:
                    send_mail(subject, Message, contact_email, [settings.EMAIL_HOST_USER],fail_silently=False)
                # send_mail("HI", Message, contact_email, [settings.EMAIL_HOST_USER],fail_silently=False)
            except BadHeaderError:
                return HttpResponse('Invalid header found.')

            return redirect("contact_us")
    return render(request, "contact-us.html", {'form': form})
